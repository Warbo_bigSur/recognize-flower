//
//  ViewController.swift
//  WhatFlower
//
//  Created by warbo on 02/11/2021.
//

import UIKit
import CoreML
import Vision
import Alamofire
import SwiftyJSON
import SDWebImage
class ViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    let wikipediaURl = "https://en.wikipedia.org/w/api.php"
//    let netWorkManager = NetWorkManager()
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    let imagePicker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = false
        imagePicker.delegate = self
    
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let userPickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
           
            guard let ciImage = CIImage(image: userPickedImage)else{
                fatalError("cannot covert to CIImage")
            }
            detect(image: ciImage)
        }
        imagePicker.dismiss(animated: true, completion: nil)
        
    }
    func detect(image: CIImage){
        guard let model = try? VNCoreMLModel(for:FlowerClassifier().model)else{
            fatalError("loading model failed")
        }
        let request = VNCoreMLRequest(model: model) { request, error in
            guard  let classification = request.results?.first as? VNClassificationObservation else{
                fatalError("Could not classifi images.")
            }
                self.navigationItem.title = classification.identifier.capitalized
            self.fetchData(flowerName: classification.identifier)
            
            
        }
        let handler = VNImageRequestHandler(ciImage: image)
        do{
            try handler.perform([request])
        }
        catch{
            print(error)
        }
       
        
    }
  
    func fetchData(flowerName:String){
        let parameters : [String:String] = [
          "format" : "json",
          "action" : "query",
          "prop" : "extracts|pageimages",
          "exintro" : "",
          "explaintext" : "",
          "titles" : flowerName,
          "indexpageids" : "",
          "redirects" : "1",
          "pithumbsize":"500"
          ]
        Alamofire.request(wikipediaURl,method: .get,parameters: parameters).responseJSON { (reponse) in
            if reponse.result.isSuccess{
                print("Got from Wikipedia")
                print(reponse)
                let flowerJSON: JSON = JSON(reponse.result.value!)
                let pageid = flowerJSON["query"]["pageids"][0].stringValue
                let flowerDescription = flowerJSON["query"]["pages"][pageid]["extract"].stringValue
                let flowerImageURL = flowerJSON["query"]["pages"][pageid]["thumbnail"]["source"].stringValue
           
                self.imageView.sd_setImage(with: URL(string: flowerImageURL))
                self.label.text = flowerDescription
            }
        }
        

    }
   
  
    @IBAction func cameraTapped(_ sender: UIBarButtonItem) {
        present(imagePicker, animated: true, completion: nil)
    }
    
    

}

